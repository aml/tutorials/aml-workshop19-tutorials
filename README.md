# AML-workshop19-tutorials

[![Binder](https://binder.cern.ch/badge_logo.svg)](https://binder.cern.ch/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Faml%2Ftutorials%2Faml-workshop19-tutorials/tutorial_v1.9)

## Technical Setup

**Note:** if you have any issues please [report them on the `hub` mattermost channel][1]

[1]: https://mattermost.web.cern.ch/it-dep/channels/hub

There are two ways to run the tutorial

### OPTION 1 Using  CERN Binder (RECOMMENDED)

Just click on the flag at the beginning of the README

* Open a terminal on the JupyterHub and type
```
bash
kinit <CERNLOGIN>
```
to have your eos in the navigation bar on the left type
```
ln -s /eos/user/<first_lettre_username>/<username> ./eos
```

### OPTION 2 Using the JupyterHub from CERN (RECOMMENDED)

* Navigate to this web page [https://hub.cern.ch](https://hub.cern.ch).
* Sign in with your CERN account.
* You can either choose the ml profile or adding as custom image
    * for CPU`gitlab-registry.cern.ch/aml/tutorials/aml-workshop19-tutorials/aml_tutorial-cpu:latest`
    * or for GPU: `gitlab-registry.cern.ch/aml/tutorials/aml-workshop19-tutorials/aml_tutorial-gpu:latest`
<!--* (`atlas-gpu` can be used as well however there are only a few GPUs available)-->
* Open a terminal on the JupyterHub and type
```
bash
kinit <CERNLOGIN>
```
to have your eos in the navigation bar on the left type
```
ln -s /eos/user/<first_lettre_username>/<username> ./eos
```


### OPTION 3 Using lxplus with port forwarding
This option is really slow since lxplus need a lot of time to convert the docker image into a singularity image.


* Log into lxplus
```
<CERNLOGIN>@lxplus.cern.ch
```
* Execute the following command to run docker image
```
singularity exec -B /eos docker://gitlab-registry.cern.ch/aml/tutorials/aml-workshop19-tutorials/aml_tutorial-cpu:latest bash
```
with the `--pwd` flag you can specify a specific startup folder
* You are now in the docker image and can start `jupyter-lab` via
```
jupyter-lab --no-browser --port <PORT>
```
please use a port with a number greater than 1023 (it won't work when 2 people use the same port at a time).

On your local machine you need to redirect the port, open a new terminal and run
```
ssh -N -f -L localhost:8888:localhost:<PORT> <CERNLOGIN>@lxplus<LXPLUSMACHINE>.cern.ch
```
Afterwards you can access the jupyter-lab via your browser by typing as url [http://localhost:8888](http://localhost:8888)

You will be asked to provide a token which can be obtained from the terminal in which you started jupyter-lab where you see a line like this
```
http://localhost:7760/?token=7d41c1e979e62653221c6018b4a23eb44bb6035fa349173c
```
You need to copy paste the token (in this case `7d41c1e979e62653221c6018b4a23eb44bb6035fa349173c` - please use yours this one will not work).

